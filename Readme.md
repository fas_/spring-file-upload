# Minimium Working Example

This samples is only a minimium working example of a file uploader and file download.

The intention is to help the pratical works in the Spring Boot Framework.

An example of the working sample website can be found in:
![alt text](./src/main/resources/example_site.png "Authorized endpoint with postman")
