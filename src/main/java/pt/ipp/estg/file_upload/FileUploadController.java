package pt.ipp.estg.file_upload;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Function;

@RestController
public class FileUploadController {

    @PostMapping(value="/upload",  consumes = MediaType.MULTIPART_FORM_DATA_VALUE  )
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<String> upload(@RequestPart("file") FilePart file, @RequestPart("texto") String txt) throws IOException {

        Path tempfile = Files.createFile(Paths.get("src\\main\\resources\\filesBD\\"+file.filename()));

        AsynchronousFileChannel channel = AsynchronousFileChannel.open(tempfile);
        DataBufferUtils.write(file.content(), channel, 0).subscribe();
        file.transferTo(tempfile.toFile());
        return Mono.just(txt);

    }

    @GetMapping("files/{fileName}")
    public Mono<Resource> getFile(@PathVariable String fileName) {
        Resource res = new ClassPathResource("\\filesBD\\"+fileName);
        return Mono.just(res);
    }

}
